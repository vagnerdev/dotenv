#!/bin/bash


###############################################################
###                         PROJECTS                        ###
############################################################### 
# Codigo42
alias cd::analytics='cd $HOME/Code/Codigo42/Analytics'
alias cd::social='cd  $HOME/Code/Codigo42/Social'
alias cd::legislativo='cd $HOME/Code/Codigo42/Legislativo'
alias cd::protocolo='cd $HOME/Code/Codigo42/Protocolo'
alias cd::educacao='cd $HOME/Code/Codigo42/Educacao'
# Suporte42
alias cd::horizon='cd $HOME/Code/Suporte42/Horizon'
alias cd::host='cd $HOME/Code/Suporte42/Host'
alias cd::security='cd $HOME/Code/Suporte42/Security'
alias cd::suporte='cd $HOME/Code/Suporte42/Suporte'
# Rocket42
alias cd::wp-core='cd $HOME/Code/Rocket42/Wordpress/Core'
alias cd::wp-plugins='cd $HOME/Code/Rocket42/Wordpress/Plugins'
alias cd::wp-themes='cd $HOME/Code/Rocket42/Wordpress/Themes'
alias cd::rocket='cd $HOME/Code/Rocket42/Projects'
# Dev42
alias cd::dotenv='cd $HOME/Code/Dev42/dotenv'
alias cd::initializr='cd  $HOME/Code/Dev42/initializr'